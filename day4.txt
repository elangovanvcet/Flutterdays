//day 4- textfield, textcontroller , themedata

class FirstPage extends StatefulWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  var emailNametext = TextEditingController();
  var emailPasstext = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("TextController"),
      ),
      body: Padding(
          padding: EdgeInsets.all(25),
          child: Column(
            children: [
              emailName(),
              emailPass(),
              submitButton(),
            ],
          )),
    );
  }

  Widget emailName() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: TextField(
        controller: emailNametext,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.mail),
          labelText: "Email",
          hintText: "Enter your email id",
          border: OutlineInputBorder(),
        ),
      ),
    );
  }

  Widget emailPass() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: TextField(
        controller: emailPasstext,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.password),
          hintText: "Enter your password",
          labelText: "Password",
          border: OutlineInputBorder(),
        ),
      ),
    );
  }

  Widget submitButton() {
    return Container(
      decoration: BoxDecoration(),
      child: ElevatedButton(
          onPressed: () {
            print("pressed");
            setState(() {
              print(emailNametext.text);
              print(emailPasstext.text);
              emailNametext.clear();
              emailPasstext.clear();
            });
          },
          child: Text("Submit")),
    );
  }
}